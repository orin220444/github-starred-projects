use serde::{Deserialize, Serialize};
use anyhow::{Result, Context};
use dotenv::dotenv;
#[tokio::main]
async fn main() -> Result<()>{
    dotenv().ok();
let octocrab = octocrab::OctocrabBuilder::new()
    .personal_token(std::env::var("GITHUB_PERSONAL_TOKEN").unwrap()).build().unwrap()
    .current()
    .list_repos_starred_by_authenticated_user()
    .send()
    .await?;
    let docs = octocrab
        .items
        .into_iter()
        .map(|repo|  GithubRepository{ name: repo.name, description: repo.description, url: repo.html_url.unwrap().to_string(), from: "github".to_string()
        })
    .map(|repo| to_document(&repo).unwrap())
    .collect::<Vec<Document>>();
    let db = connect_to_db().await?;
    let conn = db.collection::<Document>("urls");
    conn.insert_many(docs, None).await?;
    //println!("{:#?}", octocrab.items);
    Ok(())
}
#[derive(Debug, Serialize, Deserialize)]
struct GithubRepository{
    url: String,
    from: String,
    name: String,
    description: Option<String>,
}
use mongodb::{options::ClientOptions, Client, Database, bson::{Document, to_document}};
use std::time::Duration;

pub async fn connect_to_db() -> Result<Database> {
    let mut client_options = ClientOptions::parse(
        &std::env::var("MONGODB_URL")
            .context("No db url present!")
            .unwrap(),
    )
    .await?;
    let duration = Duration::new(5, 0);
    client_options.connect_timeout = Some(duration);
    let client = Client::with_options(client_options).unwrap();
    let db = client.database("scraping_urls");
    Ok(db)
}
